//
//  JadwalSholatController.swift
//  Muslim-Pocket-iOS
//
//  Created by Poernomo Santoso  on 23/01/20.
//  Copyright © 2020 KMDN. All rights reserved.
//

import UIKit


struct data {
    
    let sholat : String
    let waktu : String
    
}



class JadwalSholatController: UIViewController {
    
    var dataaa : [data] = [
        data(sholat: "Shubuh", waktu: "04:07"),
        data(sholat: "Dzuhur", waktu: "11:38"),
        data(sholat: "Ashar", waktu: "14:56"),
        data(sholat: "Maghrib", waktu: "17:49"),
        data(sholat: "Isya", waktu: "19:01")
    ]
    
    
    
    
    
        
//    let Array = ["Shubuh", "Dzuhur", "Ashar", "Maghrib", "Isya"]
//    let array2 = ["04:07", "11:38", "14:56", "17:49", "19:01"]

    @IBOutlet weak var tableViewCont: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewCont.dataSource = self
        tableViewCont.delegate = self

    }

}


extension JadwalSholatController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataaa.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let der = dataaa[indexPath.row]
        
        
        let cell = tableViewCont.dequeueReusableCell(withIdentifier: "Cell")
        
        
        let text1 = cell?.textLabel
        let text2 = cell?.detailTextLabel
        
        text1?.font = UIFont(name: "Helvetica", size: 17)
        text1?.textColor = UIColor.white
        text2?.font = UIFont(name: "Helvetica", size: 17)
        text2?.textColor = UIColor.white
        
        
        text1?.text = der.sholat
        text2?.text = der.waktu
//        cell?.detailTextLabel?.text = array2[indexPath.row]
        let label = UILabel.init(frame: CGRect(x:0,y:0,width:70,height:20))
        label.font = UIFont(name: "Helvetica", size: 17)
        label.textColor = UIColor.white
        label.text = der.waktu
        
        cell!.accessoryView = label
        
        return cell!
        
    }
    
    
}
