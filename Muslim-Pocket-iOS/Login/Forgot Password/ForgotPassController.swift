//
//  ForgotPassController.swift
//  Muslim-Pocket-iOS
//
//  Created by Poernomo Santoso  on 06/12/19.
//  Copyright © 2019 KMDN. All rights reserved.
//

import UIKit

class ForgotPassController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    

    
}

extension ForgotPassController {
    
    func hideKeyboardWhenTappedAround() {
            let tapGesture = UITapGestureRecognizer(target: self,
                             action: #selector(hideKeyboard))
            view.addGestureRecognizer(tapGesture)
        }

        @objc func hideKeyboard() {
            view.endEditing(true)
        }
    }
