//
//  Loginn.swift
//  Muslim-Pocket-iOS
//
//  Created by Poernomo Santoso  on 18/12/19.
//  Copyright © 2019 KMDN. All rights reserved.
//

import UIKit


@IBDesignable
    open class customUITextField: UITextField { //ini buat custom textfield

        func setup() {
            let border = CALayer()
            let width = CGFloat(2.0)
            border.borderColor = (UIColor(red: 43, green: 163, blue: 90, alpha: 0) as! CGColor)
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setup()
//    }
//    required public init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setup()
//    }
//}
    
}

class Loginn: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    

}

extension Loginn {

func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                         action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}


